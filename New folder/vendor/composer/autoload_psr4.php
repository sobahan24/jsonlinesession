<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Yoeriboven\\LaravelLogDb\\' => array($vendorDir . '/yoeriboven/laravel-log-db/src'),
    'Spatie\\LaravelPackageTools\\' => array($vendorDir . '/spatie/laravel-package-tools/src'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Illuminate\\Contracts\\' => array($vendorDir . '/illuminate/contracts'),
);
